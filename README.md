# Folder

cd ..
mkdir www
cd www
mkdir dev

# docker app

```bash
mv .env .evn.example
docker-compose up -d --build
```

# OS in container

docker-compose exec nginx cat /etc/os-release

# Configuration Vhost Window

C:\Windows\System32\Drivers\etc\hosts

127.0.0.1 dev.localhost

# Mysql
copy file /mariadb/docker-entrypoint-initdb.d/{example_mysql}.sql

```
docker-compose exec mariadb /bin/bash

cd docker-entrypoint-initdb.d
mysql -u userName -p --init-command="SET SESSION FOREIGN_KEY_CHECKS=0;" -f -D db_name < file_name.sql
```

# Laravel

```bash
docker-compose exec php-fpm sh

cd {folder_laravel_project}

php artisan serve --host=0.0.0.0 --port=8000
```

# Https
remove all file in /nginx/ssl
re-run docker-compose up -d --build

# NodeJs
```bash
docker-compose exec nodejs sh

cd {folder_nodejs_project}

yarn install

yarn run serve --port=3000
```

# Bug
Setting DNS 8.8.8.8 when docker run package failed

***
### Credit to Nghia Vu