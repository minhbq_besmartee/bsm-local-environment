#!/bin/bash

mkdir /etc/apache2/ssl 2> /dev/null

# Change dev.localhost to the URL to be used
if [ ${APACHE_HTTP2} = true ]; then
  if [ ! -f /etc/apache2/ssl/ssl_site.crt ]; then
    openssl genrsa -out "/etc/apache2/ssl/ssl_site.key" 2048
    openssl rand -out /root/.rnd -hex 256
    openssl req -new -key "/etc/apache2/ssl/ssl_site.key" -out "/etc/apache2/ssl/ssl_site.csr" -subj "/CN=dev.localhost/O=DevLocal/C=BR"
    openssl x509 -req -days 365 -extfile <(printf "subjectAltName=DNS:dev.localhost,DNS:*.dev.localhost") -in "/etc/apache2/ssl/ssl_site.csr" -signkey "/etc/apache2/ssl/ssl_site.key" -out "/etc/apache2/ssl/ssl_site.crt"
  fi

fi

# Start apache in foreground
/usr/sbin/apache2ctl -D FOREGROUND
